﻿namespace Book_Library_v0._8
{
    public interface ICountingBooks
    {
        void LibraryCount();
        void DepartmentCount();
        void BiggestDepartment();
        void BiggestAvtor();
        void SmallestBook();
    }
}