﻿using System;
using System.Linq;

namespace Book_Library_v0._8
{
    public static class DepartmentRepository
    {
        public static Department[] departments;
        private static int index;

        static DepartmentRepository()
        {
            departments = new Department[1000];
            index = 0;
        }

        public static void AddNewDept(Department Dept)
        {
            departments[index] = Dept;
            index++;
        }

        public static Department[] GetAllDepartments()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            var departmentArray = new Department[index];
            Array.Copy(departments, departmentArray, index);
            return departmentArray;
        }

        public static Book[] GetDepartmentBook(Department dept)
        {
            return BookRepository.GetAllBooks().Where(x => x.department.Name == dept.Name).ToArray();
        }
    }
}