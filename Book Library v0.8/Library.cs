﻿using System;
using System.Linq;

namespace Book_Library_v0._8
{
    public class Library : ICountingBooks
    {
        private int libraryCount, avtorCount, ukrDeptCount, worldDeptCount, itDeptCount;
        private Department  firstDepElement;
        private Avtor firstAvtorElement;
        private Book smallestBook;
        public void CreateBooks() // Створюємо нові книжки
        {
            Department[] Departments =
            {
                new Department("Українська лiт."), // Створюємо відділи
                new Department("Iноземна лiт."),
                new Department("IT лiт.")
            };
            foreach (var dept in Departments)
            {
                DepartmentRepository.AddNewDept(dept); // Заносимо їх в масив
            }
            Avtor[] Avtors =
            {                
                new Avtor("Панас Мирный"), // Українські автори
                new Avtor("Григорiй Квiтка-Основ'яненко"),
                new Avtor("Ольга Кобилянська"),

                new Avtor("Ги де Мопассан"), // Іноземні автори
                new Avtor("Агатa Кристи"),
                new Avtor("Джордж Орвелл"),
                new Avtor("Вiльям Шекспiр"),

                new Avtor("Уоллес Вонг"), // ІТ автори
                new Avtor("Роджер Пенроуз"),
                new Avtor("Герберт Шилдт")
            };
            foreach (var avtor in Avtors)
            {
                avtorCount++;
                AvtorRepository.AddNewAvtor(avtor);
            }            
            Book[] Books =
            {
                new Book(Departments[0], Avtors[0], "Хiба ревуть воли, як ясла повнi?", 336), // Українські книжки
                new Book(Departments[0], Avtors[0], "Повiя", 512),
                new Book(Departments[0], Avtors[0], "Гулящая: Роман из народной жизни", 560),
                new Book(Departments[0], Avtors[1], "Маруся", 608),
                new Book(Departments[0], Avtors[1], "Конотопська вiдьма", 488),
                new Book(Departments[0], Avtors[2], "Царiвна", 350),
                new Book(Departments[0], Avtors[2], "В недiлю рано зiлля копала...", 488),

                new Book(Departments[1], Avtors[3], "Бродячая жизнь", 224), // Іноземні книжки
                new Book(Departments[1], Avtors[3], "Милый друг", 76),
                new Book(Departments[1], Avtors[4], "Загадочное происшествие в Стайлзе", 288),
                new Book(Departments[1], Avtors[4], "Приключение рождественского пудинга", 86),
                new Book(Departments[1], Avtors[4], "Пассажир из Франкфурта", 320),
                new Book(Departments[1], Avtors[5], "Дни в Бирме", 352),
                new Book(Departments[1], Avtors[5], "Фунты лиха в Париже и Лондоне", 224),
                new Book(Departments[1], Avtors[6], "Ромео i Джульєтта", 208),

                new Book(Departments[2], Avtors[7], "Основы программирования для чайников", 336), // ІТ книжки
                new Book(Departments[2], Avtors[7], "Microsoft Office 2010 для чайников", 368),
                new Book(Departments[2], Avtors[8], "Новый ум короля. О компьютерах мышлении", 339),
                new Book(Departments[2], Avtors[8], "Тени разума: В поисках науки о сознании", 688),
                new Book(Departments[2], Avtors[9], "C# 4.0: полное руководство", 1056),
                new Book(Departments[2], Avtors[9], "Полный справочник по C++, 4-е издание", 800),
                new Book(Departments[2], Avtors[9], "Java: методики программирования Шилдта", 512),
                new Book(Departments[2], Avtors[9], "C: полное руководство, классическое издание", 704),
                new Book(Departments[2], Avtors[9], "Программирование на BORLAND C++", 800)
            };
            foreach (var Book in Books)
            {
                libraryCount++;
                BookRepository.AddNewBook(Book);
            }
        }
        public void ShowBooks() // Показати всі книжки
        {
            Console.Clear();

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Бiблiотека має три вiддiли: \nУкараїнська лiтература, Iноземна лiтература, IТ лiтература\n\n");
            Console.ForegroundColor = ConsoleColor.Green;

            foreach (var books in BookRepository.GetAllBooks())
            {
                Console.WriteLine(books);
            }

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nВiддiли – за кiлькiстю книжок у вiддiлi:\n");
            var depts = DepartmentRepository.GetAllDepartments();
            Array.Sort(depts);
            //string.Join(",  ", depts.ToList());
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(string.Join(", ", depts.ToList()));

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nАвтори – за кiлькiстю виданих книжок:\n");
            var avtors = AvtorRepository.GetAllAvtors();
            Array.Sort(avtors);
            //string.Join(",  ", avtors.ToList());
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(string.Join("\n", avtors.ToList()));

            var sortbooks = BookRepository.GetAllBooks();
            Array.Sort(sortbooks);

            firstDepElement = depts.First(); // Заносимо перші елементи з відсортованого масиву
            firstAvtorElement = avtors.First();
            smallestBook = sortbooks.First();

            DepartmentCount();
            LibraryCount();

            BiggestDepartment();
            BiggestAvtor();
            SmallestBook();

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nКнижки – за кiлькiстю сторiнок у книжцi:\n");
            Console.ForegroundColor = ConsoleColor.Green;
            foreach (var book in sortbooks)
            {
                Console.WriteLine(book);
            }

            Question();
        }
        public void Question()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\nБажаєте додати нову книжку?\n \t  Y/N");
            Console.ForegroundColor = ConsoleColor.Green;
       startQuestion:
            string anser = Console.ReadLine();
            if ((anser == "y") || (anser == "Y")) AddBook();
            else if ((anser == "n") || (anser == "N")) DogRun();
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Для вiдповiдi необхiдно ввести або y - так або n - нi");
                Console.ForegroundColor = ConsoleColor.Green;
                goto startQuestion;
            }
        }
        public void AddBook()
        {
            string newAvtor, newBook, department;
            int pages;

            Console.WriteLine("Вкажiть повне iмя автора:");
            newAvtor = Console.ReadLine();
            Console.WriteLine("Вкажiть повну назву книжки:");
            newBook = Console.ReadLine();
       start1:
            try
            {
                Console.WriteLine("Вкажiть кiлькiсть сторiнок у книжцi:");
                pages = Convert.ToInt32(Console.ReadLine());
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Помилка вводу! попробуйте ще раз...\n");
                Console.ForegroundColor = ConsoleColor.Green; goto start1;
            }
            Console.WriteLine("Вкажiть вiддiл: Ukraine literature, World literature, IT literature\n(можливе скорочення ukr / world / IT)\n");
       start2:
            department = Console.ReadLine();
            if ((department == "Ukraine literature") || (department == "ukr") || (department == "UKR"))
            {
                AvtorRepository.AddNewAvtor(AvtorRepository.avtors[avtorCount] = new Avtor(newAvtor));
                BookRepository.books[libraryCount] = new Book(DepartmentRepository.departments[0], AvtorRepository.avtors[avtorCount], newBook, pages);
                BookRepository.AddNewBook(BookRepository.books[libraryCount]);
                libraryCount++;
                avtorCount++;
            }
            else if ((department == "World literature") || (department == "world") || (department == "WORLD"))
            {
                AvtorRepository.AddNewAvtor(AvtorRepository.avtors[avtorCount] = new Avtor(newAvtor));
                BookRepository.books[libraryCount] = new Book(DepartmentRepository.departments[1], AvtorRepository.avtors[avtorCount], newBook, pages);
                BookRepository.AddNewBook(BookRepository.books[libraryCount]);
                libraryCount++;
                avtorCount++;
            }

            else if ((department == "IT literature") || (department == "it") || (department == "IT"))
            {
                AvtorRepository.AddNewAvtor(AvtorRepository.avtors[avtorCount] = new Avtor(newAvtor));
                BookRepository.books[libraryCount] = new Book(DepartmentRepository.departments[2], AvtorRepository.avtors[avtorCount], newBook, pages);
                BookRepository.AddNewBook(BookRepository.books[libraryCount]);
                libraryCount++;
                avtorCount++;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Помилка вводу! попробуйте ще раз...\n");
                Console.ForegroundColor = ConsoleColor.Green;
                goto start2;
            }

            ShowBooks();
        }
        
        public void LibraryCount() // Реалізуємо інтерфейс ICountingBooks
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("\nЗагалом у бiблiотецi {0} книжки\n", libraryCount);
            Console.ForegroundColor = ConsoleColor.Green;
        }
        public void DepartmentCount()
        {
            ukrDeptCount = worldDeptCount = itDeptCount = 0;
            foreach (var a in DepartmentRepository.GetDepartmentBook(DepartmentRepository.departments[0])) // Заносимо по кількості книжок у відділі
            { ukrDeptCount++; }
            foreach (var a in DepartmentRepository.GetDepartmentBook(DepartmentRepository.departments[1]))
            { worldDeptCount++; }
            foreach (var a in DepartmentRepository.GetDepartmentBook(DepartmentRepository.departments[2]))
            { itDeptCount++; }
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("\nУ вiддiлi Українська лiтература {0} книжки\nУ вiддiлi Iноземна лiтература {1} книжки\nУ вiддiлi IТ лiтература {2} книжки\n", ukrDeptCount, worldDeptCount, itDeptCount);
            Console.ForegroundColor = ConsoleColor.Green;
        }
        public void BiggestDepartment()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("\nВiддiл у якому найбiльше книжок: {0}\n", firstDepElement);
            Console.ForegroundColor = ConsoleColor.Green;
        }
        public void BiggestAvtor()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("\nАвтор у якого найбiльше книжок: {0}\n", firstAvtorElement);
            Console.ForegroundColor = ConsoleColor.Green;

        }
        public void SmallestBook()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nКнижка, яка має найменшу кiлькiсть сторiнок у всiй бiблiотецi: \n{0}", smallestBook);
            Console.ForegroundColor = ConsoleColor.Green;
        }

        public void DogRun()
    {
        while (true)
        {
            Console.SetCursorPosition(0, 11);
            Console.ForegroundColor = ConsoleColor.Cyan;

            Console.WriteLine("                                          " + (char)30);
            Console.WriteLine("                                    \\     |@_  ");
            Console.WriteLine("                                     =====|    ");
            Console.WriteLine("                                     \\  /    ");
            Console.WriteLine("                                     / /     ");
            Console.SetCursorPosition(0, 16);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=-");
            System.Threading.Thread.Sleep(100);


            Console.SetCursorPosition(0, 11);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("                                          " + (char)30);
            Console.WriteLine("                                    \\     |@_  ");
            Console.WriteLine("                                     =====|___ ");
            Console.WriteLine("                                     \\       ");
            Console.WriteLine("                                     /        ");
            Console.SetCursorPosition(0, 16);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- ");
            System.Threading.Thread.Sleep(100);


            Console.Clear();
            Console.SetCursorPosition(0, 10);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("                                          " + (char)30);
            Console.WriteLine("                                    \\     |@_   ");
            Console.WriteLine("                                     =====|___   ");
            Console.WriteLine("                                    /          ");
            Console.WriteLine("                                   /           ");
            Console.SetCursorPosition(0, 16);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write(" -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -");
            System.Threading.Thread.Sleep(100);


            Console.Clear();
            Console.SetCursorPosition(0, 9);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("                                          " + (char)30);
            Console.WriteLine("                                    \\     |@_   ");
            Console.WriteLine("                                  ___=====|___   ");
            Console.WriteLine("                                               ");
            Console.WriteLine("                                               ");
            Console.SetCursorPosition(0, 16);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("-=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=");
            System.Threading.Thread.Sleep(100);


            Console.Clear();
            Console.SetCursorPosition(0, 10);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("                                          " + (char)30);
            Console.WriteLine("                                    \\     |@_   ");
            Console.WriteLine("                                  ___=====|      ");
            Console.WriteLine("                                          \\     ");
            Console.WriteLine("                                           \\    ");
            Console.SetCursorPosition(0, 16);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=-");
            System.Threading.Thread.Sleep(100);


            Console.Clear();
            Console.SetCursorPosition(0, 11);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("                                          " + (char)30);
            Console.WriteLine("                                    \\     |@_   ");
            Console.WriteLine("                                  ___=====|      ");
            Console.WriteLine("                                         /     ");
            Console.WriteLine("                                         \\    ");
            Console.SetCursorPosition(0, 16);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- ");
            System.Threading.Thread.Sleep(100);
        }
    }
    }
}