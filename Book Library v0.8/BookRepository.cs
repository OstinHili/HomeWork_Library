﻿using System;

namespace Book_Library_v0._8
{
    static class BookRepository
    {
        public static Book[] books;
        private static int index;

        static BookRepository()
        {
            books = new Book[1000];
            index = 0;
        }

        public static void AddNewBook(Department department, Avtor avtor, string book, int pages)
        {
            var newBook = new Book(department, avtor, book, pages);
            books[index] = newBook;
            index++;
        }

        public static void AddNewBook(Book book)
        {
            books[index] = book;
            index++;
        }

        public static Book[] GetAllBooks()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            var bookArray = new Book[index];
            Array.Copy(books, bookArray, index);
            return bookArray;
        }
    }
}