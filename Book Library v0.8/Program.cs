﻿namespace Book_Library_v0._8
{
    class Program
    {
        static void Main(string[] args)
        {
            Library library = new Library();
            library.CreateBooks();
            library.ShowBooks();
        }
    }
}
