﻿using System;

namespace Book_Library_v0._8
{
    public class Avtor : IComparable<Avtor>
    {
        public Avtor()
        {
            this.Name = "default"; // конструктори без параметрів
        }
        public Avtor(string name)
        {
            this.Name = name;
        }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public int CompareTo(Avtor other)
        {
            if (other == null) return -1;
            var booksCount = AvtorRepository.GetAvtorBook(this).Length;
            var otherAvtorBooks = AvtorRepository.GetAvtorBook(other).Length;

            if (booksCount < otherAvtorBooks) return 1;
            if (booksCount == otherAvtorBooks) return 0;
            return -1;
        }
    }
}