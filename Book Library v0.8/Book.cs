﻿using System;

namespace Book_Library_v0._8
{
    public class Book : IComparable<Book>
    {
        public Department department { get; set; }
        public Avtor avtor { get; set; }
        public string name { get; set; }
        public int pages { get; set; }

        public Book(Department department, Avtor avtor, string bookName, int pages)
        {
            this.department = department;
            this.avtor = avtor;
            this.name = bookName;
            this.pages = pages;
        }
        public override string ToString()
        {
            return department.Name + (char)26 + " " + avtor.Name + ": " + (char)34 + name + (char)34 + " - " + pages + "ст.";            
        }

        public int CompareTo(Book obj)
        {
            if (this.pages < obj.pages) return -1;
            if (this.pages == obj.pages) return 0;
            return 1;
        }
    }
}