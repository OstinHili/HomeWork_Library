﻿using System;
using System.Linq;

namespace Book_Library_v0._8
{
    public static class AvtorRepository
    {
        public static Avtor[] avtors;
        private static int index;

        static AvtorRepository()
        {
            avtors = new Avtor[1000];
            index = 0;
        }

        public static void AddNewAvtor(Avtor avtor)
        {
            if (GetAllAvtors().Any(x => x.Name == avtor.Name)) // провірка на наявність подібного автора... 
            {
                avtors[index] = avtor;
            }
            else
            {
                avtors[index] = avtor;
                index++;
            }
        }

        public static Avtor[] GetAllAvtors()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            var departmentArray = new Avtor[index];
            Array.Copy(avtors, departmentArray, index);
            return departmentArray;
        }

        public static Book[] GetAvtorBook(Avtor avtor)
        {
            return BookRepository.GetAllBooks().Where(x => x.avtor.Name == avtor.Name).ToArray();
        }
    }
}