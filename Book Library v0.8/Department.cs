﻿using System;

namespace Book_Library_v0._8
{
    public class Department : IComparable<Department>
    {
        public Department()
        {
            Name = "default";
        }

        public Department(string Name)
        {
            this.Name = Name;
        }

        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }


        public int CompareTo(Department other)
        {
            if (other == null) return -1;
            var booksCount = DepartmentRepository.GetDepartmentBook(this).Length;
            var otherAvtorBooks = DepartmentRepository.GetDepartmentBook(other).Length;

            if (booksCount < otherAvtorBooks) return 1;
            if (booksCount == otherAvtorBooks) return 0;
            return -1;
        }
    }
}